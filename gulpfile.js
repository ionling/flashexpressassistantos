var gulp = require('gulp');
var babel = require("gulp-babel");

// all js files of this project
var scriptsGlob = './src/**/*.js'
// all other files except scripts
var otherGlobs = ['./src/**/*', '!./src/**/*.js',
    '!./src/.vscode', '!./src/.eslintrc.json'
]
// all the files that weapp needs(just scriptsGlob + otherGlobs)
var allGlobs = ['./src/**/*', '!./src/.vscode', '!./src/.eslintrc.json']
// The path (output folder) to write files to
var dest = './dist'
// option for gulp.src()
const base = './src'

// use babel to compile js file
var buildScript = (globs) =>
    gulp.src(globs, {
        base: base
    })
    .pipe(babel())
    .pipe(gulp.dest(dest))


var copy = (globs) =>
    gulp.src(globs, {
        base: base
    })
    .pipe(gulp.dest(dest))

gulp.task('default')

// 编译 js
gulp.task('scripts', () =>
    buildScript(scriptsGlob)
)

// 复制其他的文件
gulp.task('copyOther', () =>
    copy(otherGlobs)
)

// 执行所有的构建
gulp.task('build', ['scripts', 'copyOther'], () => {})

// Watch files and do something when a file changes.
gulp.watch(allGlobs, (event) => {
    /**
     * 获取相对路径，像 `.\src\**`
     * @param {string} path
     */
    const getRelativePath = (path) => {
        const i = path.lastIndexOf('\\src\\')
        return '.' + path.substring(i)
    }
    const path = event.path
    const type = event.type
    console.log('File ' + path + ' was ' + type + ', running tasks...');
    if (type != "changed")
        return
    const glob = getRelativePath(path)
    const postfix = glob.slice(glob.length - 3)
    const isScript = '.js' == postfix
    console.log('is a script:', isScript)
    if (isScript)
        buildScript(glob)
    else
        copy(glob)
})
