// 日志记录模块
// 日志将保存在 Bmob
const regeneratorRuntime = require('./runtime')
const Bmob = require('./bmob')
const util = require('./util')

console.log('getApp in log.js')
const Log = Bmob.Object.extend('Log')
const level = {
    trace: 10,
    debug: 20,
    info: 30,
    warn: 40,
    error: 50,
    fatal: 60
}

/**
 * If a log's level less than the sendLevel, it wont be send to bmob
 */
const sendLevel = level.warn

function trace(...message) {
    log(level.trace, message)
}

function debug(...message) {
    log(level.debug, message)
}

function info(...message) {
    log(level.info, message)
}

function warn(...message) {
    log(level.warn, message)
}

function error(...message) {
    log(level.error, message)
}

function fatal(...message) {
    log(level.fatal, message)
}

/**
 * According to level, log will send information to bmob,
 * but always output to console.
 * @param {number} level
 * @param {array} message
 */
function log(level, message) {
    if (level >= sendLevel)
        send(level, message)
    console.log(level, message)
}

/**
 * Send information to bmob.
 * @param {number} level
 * @param {array} message
 */
const send = async (level, message) => {
    const globalData = getApp().globalData
    let log = new Log()
    let systemInfo
    try {
        systemInfo = await util.getSystemInfo()
    } catch (error) {
        systemInfo = error
    }
    log.save({
        user: globalData.user,
        level: level,
        message: message,
        status: 2,
        systemInfo: systemInfo,
        appVersion: globalData.version,
    })
    console.log('send to bmob successfully')
}

module.exports = {
    trace: trace,
    debug: debug,
    info: info,
    warn: warn,
    error: error,
    fatal: fatal
}
