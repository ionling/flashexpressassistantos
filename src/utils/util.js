/**
 * Format time YYYY-MM-DD
 * @param {Date} date Date
 */
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return `${year}-${month}-${day}`
}

/**
 * Make date string format work well in JavaScriptCore.
 * @param {String} s String in format of YYYY-MM-DD
 */
const parseTime = s => {
  return s.replace(/-/g, "/")
}

/**
 * Encapsulate wx.getSystemInfo with Promise.
 */
const getSystemInfo = () => {
  return new Promise((resolve, reject) => {
    wx.getSystemInfo({
      success: function (res) {
        resolve(res)
      },
      fail: function (res) {
        reject(res)
      }
    })
  })
}

/**
 * Encapsulate wx.getNetworkType with Promise.
 */
const getNetworkType = () => {
  return new Promise((resolve, reject) => {
    wx.getNetworkType({
      success: function (res) {
        resolve(res)
      },
      fail: function (res) {
        reject(res)
      }
    })
  })
}

module.exports = {
  formatTime: formatTime,
  parseTime: parseTime,
  getSystemInfo: getSystemInfo,
  getNetworkType: getNetworkType,
}
