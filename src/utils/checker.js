let app = getApp()

// 检查表单提交的地址是否正确，支持promise
const checkAddress = value => {
    try {
        if (value.addressee == "")
            throw "收件人不能为空"
        else if (value.phone == "")
            throw "手机不能为空"
        else if (value.phone.length != 11)
            throw "手机号码尾数不对"
        // 可能存在教师宿舍情况，和寝室有关的部分就不check了
    } catch (error) {
        return app.bmob.Promise.error(error)
    }
    return app.bmob.Promise.as("地址正确")
}

const checkFeedback = value => {
    // QQ 选填
    if (value.content == "")
        return app.bmob.Promise.error('请填写您的反馈或建议')
    else
        return app.bmob.Promise.as('反馈正确')
}

module.exports = {
    checkAddress: checkAddress,
    checkFeedback: checkFeedback
}
