//app.js
const Bmob = require('utils/bmob.js')
const regeneratorRuntime = require('utils/runtime')
const log = require('utils/log')
const util = require('utils/util')

Bmob.initialize("your bmob appid", "your bmob rest api key")


App({
	/**
	 * 全局数据
	 */
	globalData: {
		version: '1.2.13',
		userInfo: {},
		// addresses: [],
		// currentAddress: {},
		// // 是不是第一个添加的地址，如果是的话设为默认地址
		// firstAddress: false,
		// orders: [],
		// currentOrder: {},
		// // 当前用户的默认地址
		// defaultAddress: {}
	},

	/**
	 * 对一些Bmob对象提供快捷访问
	 */
	bmob: {
		// warning 不能定义为App的成员
		// 因为 this 引用的关系
		Address: Bmob.Object.extend("Address"),
		Order: Bmob.Object.extend("Order"),
		Sending: Bmob.Object.extend("Sending"),
		Fetching: Bmob.Object.extend("Fetching"),
		Feedback: Bmob.Object.extend("Feedback"),
		Honey: Bmob.Object.extend("Honey"),
		Data: Bmob.Object.extend("Data"),
		Query: Bmob.Query,
		Pay: Bmob.Pay,
		User: Bmob.User,
		Promise: Bmob.Promise
	},

	onLaunch: async function () {
		// first step
		wx.showLoading({
			title: '初始化中'
		})
		// 1. check network type
		try {
			const res = await util.getNetworkType()
			log.info(res)
			if (res.networkType == 'none') {
				wx.showModal({
					title: '提示',
					content: '网络出错，小闪无法为您服务',
					showCancel: false
				})
			}
		} catch (error) {
			log.info(error)
		}

		// 2. check weapp version
		const version = this.globalData.version
		try {
			const value = wx.getStorageSync('version')
			// 如果不存在指定的键，value = ""
			// 所以遇到两种情况：版本号不一致，没有本地版本缓存
			// 都会进行更新操作
			if (value != version) {
				// 会清楚 Bmob 的 User 缓存
				wx.clearStorageSync()
				wx.setStorage({
					key: "version",
					data: version,
					fail: (e) => {
						this.log('info', 'error in checking weapp version step:', e)
					}
				})

				this.showHint(`欢迎使用闪电速代助手 v${version}，如遇任何使用问题，可进入我的-关于页面清空缓存重试`)
				log.info('info', 'upgrade weapp succeed')
				// Do something with return value
			}
		} catch (e) {
			log.warn('exception in checking weapp version:', e)
		}

		// 3. user logs in and init app data
		await this.getUserInfo()
		let user = Bmob.User.current();
		if (!user) {
			try {
				user = await this.logIn()
			} catch (error) {
				this.showHint('用户登录失败，可能会造成使用异常，可进入我的-关于页面清空缓存重试')
				return
			}
		}
		await this.initializeAppData(user)

		// final step
		wx.hideLoading()
		// log.error('test')
        console.log('app init complete')
	},

	/**
	 * 进行App数据初始化
	 * @param {*} user - Bmob.User
	 */
	initializeAppData: async function (user) {
		let app = this

		// 必须先进行初始化globalData.user属性
		// 否则初始化其他数据时可能出现user === undefined
		// 导致把其他用户的信息都拉取下来
        app.globalData.user = user
		// 这里采用每次启动 app 都需要进行一次地址请求
		// 因为订单提交需要 Bmob.Object 类型的地址
		await app.queryAddress()
		await app.queryOrders()
		app.checkOrders()
	},

	/**
	 * 获取scope.userInfo权限
	 * 如果失败，将调用getUserInfo()重新弹出对话框要求用户进行选择
	 */
	getAuthority: function () {
		console.log('call getAuthority()')
		wx.showModal({
			title: "需要获取用户信息",
			content: "为了实现订单功能，小闪需要您的用户信息，点击确认进入设置修改界面！",
			showCancel: false,
			success: () => {
				wx.openSetting({
					success: (res) => {
						if (res.authSetting["scope.userInfo"] == true) {
							console.log("ok get authority")
						}
						this.getUserInfo()
					}
				})
			},
		})
	},

	/**
	 * 通过wx.getUserInfo 接口获取 nickName等值，并存在globalData.userInfo里
	 * 失败将会调用 getAuthority()
	 * 支持 Promise
	 */
	getUserInfo: function () {
		return new Promise(resolve => {
			wx.getUserInfo({
				success: (res) => {
					console.log('succeed in wx.getUserInfo()', res)
					const info = res.userInfo
					this.globalData.userInfo = {
						nickName: info.nickName,
						avatarUrl: info.avatarUrl,
						gender: info.gender,
						city: info.city
					}
					resolve()
				},
				// 用户点击拒绝将会调用fail函数
				fail: this.getAuthority
			})
		})
	},

	/**
	 * 新版本的logIn
	 * @returns {Promise} Bmob.User
	 */
	logIn: function () {
		const app = this
		return new Promise((resolve, reject) => {
			// bug 调用logIn() 时候函数并没有被初始化
			wx.login({
				success: async function (res) {
					try {
						let user = new app.bmob.User()
						user = await user.loginWithWeapp(res.code)
						console.log('call Bmob.User.prototype.loginWithWeapp() succeed!', user)

						if (user.get("nickName")) {
							// 登录成功
							console.log('user log in succeed')
						} else {
							console.log('into signing up flow!')
							const userInfo = app.globalData.userInfo;
							user.set('nickName', userInfo.nickName);
							user.set("avatarUrl", userInfo.avatarUrl);
							user.set("gender", userInfo.gender);
							user.set("city", userInfo.city);
							await user.save()
							console.log('succeed in signing up flow!')
						}
						resolve(user)
					} catch (error) {
						log.error('wrong with user.loginWithWeapp()', error)
						reject(error)
					}
				}
			})
		})
	},

	/**
	 * 从云端拉取 Address 数据并存入 App.globalData
	 */
	queryAddress: async function () {
		// warning: 不能采用箭头函数的方式
		const user = this.globalData.user
		if (!user) {
			this.showHint('登录失败，请进入我的-关于页面清空缓存重试')
			return
		}

		const query = new Bmob.Query(this.bmob.Address)
		query.equalTo("user", user)
		// For compatibility, not use query.equalTo('status', 1)
		query.notEqualTo("status", 0)
		try {
			const results = await query.find()
			const data = this.globalData
			data.defaultAddress = results.find(address => address.get('isDefault') == true)
			// here globalData 不能被赋值，但是它的属性可以进行修改
			data.addresses = results
			console.log("queryAddress() succeed!", results)
			return "queryAddress() succeed!"
		} catch (error) {
			log.warn('error in queryAddress:', error)
			return Promise.reject(error)
		}
	},

	/**
	 * 从云端拉取 Order 数据并存入 App.globalData
	 */
	queryOrders: async function () {
		const user = this.globalData.user
		if (!user) {
			this.showHint('你还未登录，请进入我的-关于页面清空缓存重新登录')
			return
		}

		const query = new Bmob.Query(this.bmob.Order)
		query.equalTo("user", user);
		query.descending('updatedAt')
		try {
			const orders = await query.find()
			this.globalData.orders = orders
			log.info("queryOrders() succeed!", orders)
			return "queryOrders() succeed!"
		} catch (error) {
			const res = await util.getNetworkType()
			log.warn('error in queryOrders()', error, 'Network Type is ' + res.networkType)
			return Promise.reject(error)
		}
	},

	/**
	 * 进行订单检查，如果存在未支付订单，则前往 order index page
	 */
	checkOrders: function () {
		const orders = this.globalData.orders
		const paid = orders.every(x => x.get('status') != '未支付')
		if (!paid) {
			wx.showModal({
				title: '提示',
				content: '你还存在未支付订单，请前往支付',
				showCancel: false,
				success: (res) => {
					this.log('info', 'checkOrders showModal succeed', res)
					// 绝对路径一定要以/ 开头
					wx.navigateTo({
						url: '/pages/order/index'
					})
				}
			})
		}
	},

	/**
	 * 封装方法：显示 loading 提示框
	 */
	showLoading: (title) => {
		wx.showLoading({
			title: title,
			mask: true
		})
	},

	/**
	 * 封装方法：在一定interval之后隐藏 loading 提示框，并显示一个Toast
	 * 并执行func函数
	 */
	hideLoading: (content, interval, callback, showToast) => {
		setTimeout(function () {
			wx.hideLoading()
			if (showToast) {
				wx.showToast({
					title: content,
					icon: 'success',
					duration: 500,
					mask: true
				})
			}
			if (callback)
				callback()
		}, interval)
	},

	/**
	 * 显示提示框
	 * 采用 wx.showModal 实现，showCancel = false
	 */
	showHint: (content) => {
		wx.showModal({
			title: '提示',
			content: content,
			showCancel: false
		})
	},

	/**
	 * 封装的log方法，以提供扩展
	 */
	log: function (type, ...infos) {
		console.warn('warn: App.log() method would be removed in the future. Please use log module.')
		console.log(infos)
	}
})
