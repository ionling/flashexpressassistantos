let checker = require('../../../../utils/checker')
const regeneratorRuntime = require('../../../../utils/runtime')
const log = require('../../../../utils/log')

let app = getApp()

Page({
    formSubmit: async(e) => {
        let value = e.detail.value
        // [*] 需要对值进行检查
        // [*] 写入本地缓存
        // [*] 同步页面渲染：wx.navigateBack返回前一个页面
        //     前一个页面的 onShow 会重新用 globalData 渲染页面
        // .....
        app.showLoading("添加中")

        // 1. check data format
        try {
            await checker.checkAddress(value)
        } catch (error) {
            app.hideLoading("添加出错", 200, null, false)
            wx.showModal({
                title: '提示',
                content: error,
                showCancel: false,
            })
            return
        }

        // 2. submit data
        try {
            let address = new app.bmob.Address()
            address = await address.save({
                user: app.globalData.user,
                status: 1,
                addressee: value.addressee,
                phone: +value.phone,
                apartment: +value.apartment,
                unit: +value.unit,
                dormitory: +value.dormitory,
                // 添加的是第一个地址的话设为默认地址
                isDefault: app.globalData.firstAddress,
                other: value.other
            })
            console.log("address.save() succeed!:", address)
            await app.queryAddress()
            console.log("add address succeed")
            app.hideLoading("添加成功", 200, wx.navigateBack)
        } catch (error) {
            const info = '添加出错'
            app.hideLoading(info, 200, null, false)
            log.warn(info, 'in add address page', error)
            app.showHint(info)
        }
    }
})
