let app = getApp()
let checker = require('../../../../utils/checker')
const regeneratorRuntime = require('../../../../utils/runtime')
const log = require('../../../../utils/log')

Page({
  data: {
    // 设计UI时的临时数据
    // address: {
    //   addressee: "张三",
    //   phone: "15328403080",
    //   apartment: 11,
    //   unit: 1,
    //   dormitory: 423,
    //   isDefault: true,
    //   other: ""
    // }
  },
  onLoad: function () {
    this.setData({
      address: app.globalData.currentAddress
    })
  },
  submit: function (e) {
    let value = e.detail.value
    wx.showModal({
      title: '提示',
      content: '修改完成?',
      success: async res => {
        if (res.confirm) {
          console.log("用户点击确定")
          app.showLoading("添加中")

          // 1. check data format
          try {
            await checker.checkAddress(value)
          } catch (error) {
            app.hideLoading("添加出错", 200, null, false)
            wx.showModal({
              title: '提示',
              content: error,
              showCancel: false,
            })
            return
          }

          let step = 'Before address.save()'
          // 2. update data
          try {
            const address = this.data.address
            await address.save({
              addressee: value.addressee,
              phone: +value.phone,
              apartment: +value.apartment,
              unit: +value.unit,
              dormitory: +value.dormitory,
              other: value.other
            })
            step = 'After address.save()'
            log.info("success address.save()!")
            await app.queryAddress()
            app.hideLoading("修改成功", 1000, wx.navigateBack)
            log.info("修改成功")
          } catch (error) {
            const info = '提交出错'
            app.hideLoading(info, 200, null, false)
            log.warn(info, 'in address detail submit()', error, 'Step info: ' + step)
            app.showHint(info)
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  delete: function () {
    const address = this.data.address
    if (address.get('isDefault')) {
      app.showHint('默认地址无法删除')
      return
    }
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？?',
      success: async (res) => {
        if (res.confirm) {
          console.log("用户点击确定")
          app.showLoading("删除中")
          try {
            await address.save({
              status: 0
            })
            console.log('succeed to change address.status to 0')
            await app.queryAddress()
            app.hideLoading("删除成功", 500, wx.navigateBack)
          } catch (error) {
            const info = '删除失败'
            app.hideLoading(info, 500, wx.navigateBack)
            log.warn(info, 'in address detail page', error)
          }
        }
      }
    })
  }
})
