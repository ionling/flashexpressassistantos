const regeneratorRuntime = require('../../utils/runtime')
const log = require('../../utils/log')

let app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // UI design temp data
    // addresses: [{
    //   addressee: "张三",
    //   phone: "15328403080",
    //   apartment: 11,
    //   unit: 1,
    //   dormitory: 423,
    //   isDefault: true,
    //   other: ""
    // }, {
    //   addressee: "李四",
    //   phone: "13982446090",
    //   apartment: 1,
    //   unit: 1,
    //   dormitory: 223,
    //   isDefault: false,
    //   other: "我住教师公寓29栋209",
    // }]
  },

  onShow: function () {
    this.refresh()
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    app.queryAddress().then(() => {
      this.refresh()
      wx.stopPullDownRefresh()
    })
  },

  /**
   * 利用globalData重新渲染页面
   */
  refresh: function () {
    // warning: 不能用箭头函数，否则 this 为 undefined
    this.setData({
      addresses: app.globalData.addresses
    })
  },

  goChangeState: function (e) {
    let i = parseInt(e.target.id)
    app.globalData.currentAddress = this.data.addresses[i]
    wx: wx.navigateTo({
      url: './pages/address-detail/address-detail',
    })
  },
  goAddAddress: function () {
    // 设置添加的是第一个地址为true
    if (app.globalData.addresses.length == 0)
      app.globalData.firstAddress = true
    else
      app.globalData.firstAddress = false

    if (app.globalData.addresses.length == 8) {
      wx.showModal({
        title: '提示',
        content: '抱歉，一个用户最多有8个地址，请删除你不需要的地址',
        showCancel: false
      })
    } else {
      wx: wx.navigateTo({
        url: './pages/add-address/add-address'
      })
    }
  },

  // 改变默认地址
  switchChange: async function (e) {
    // 1. 无法更改已经选中的元素
    if (e.detail.value == false) {
      this.refresh()
      return
    }
    app.showLoading("修改中")

    try {
      // 2. 取消现在的默认地址
      let cancelQuery = new app.bmob.Query(app.bmob.Address)
      cancelQuery.equalTo("user", app.globalData.user)
      cancelQuery.equalTo("isDefault", true)
      // 反正只有一个默认地址，用first应该也没有啥错误
      const address = await cancelQuery.first()
      if (address) {
        await address.save({
          isDefault: false
        })
        app.log('info', "取消默认地址成功", address)
      } else {
        app.log('warn', '不存在 isDefault 为 true 的地址')
      }

      // 3. 设置 isDefault
      const query = new app.bmob.Query(app.bmob.Address)
      const i = e.currentTarget.id
      const oldAddress = this.data.addresses[i]
      const newAddress = await query.get(oldAddress.id)
      await newAddress.save({
        isDefault: true
      })
      console.log("succeed in saving new default address")
      // 确保云端已更新上传的数据再进行请求
      // 手机端调试结果表明queryAddress的数据还要延迟一点
      // hideLoading 可以少一点时间
      setTimeout(async() => {
        await app.queryAddress()
        this.refresh()
        app.hideLoading("修改完成", 300, this.refresh)
        console.log("修改默认地址完成")
      }, 900)
    } catch (error) {
      const info = '修改默认地址出错'
      app.hideLoading(info, 400, this.refresh)
      log.warn(info, 'in address index switchChange()', error)
    }
  }
})
