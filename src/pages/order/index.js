const regeneratorRuntime = require('../../utils/runtime')
let app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // UI design tmp data
    // orders: [{
    //   orderType: "代寄快递",
    //   price: "1.00元",
    //   status: "已完成",
    //   createdAt: "2017-10-19-17:30",
    // }, {
    //   orderType: "代取快递",
    //   price: "1.90元",
    //   status: "已取货",
    //   createdAt: "2017-10-20-17:30",
    // }]
  },
  onLoad: function () {
    app.showLoading("加载中")
    app.queryOrders().then(() => {
      // warning: 尽管在 onShow 中已经调用了 refresh 方法
      // 因为网络请求是异步的，并且有延迟，所以这里一需要 call refresh()
      this.refresh()
      app.hideLoading("加载成功", 200)
    }, () => {
      app.hideLoading("失败", 200, null, false)
      console.log('cant queryOrders() in order index page')
    })
  },
  onUnload: function () {
    // 确认用户已经进行了支付
    app.checkOrders()
  },
  onShow:function () {
    this.refresh()
  },
  // 下拉刷新
  onPullDownRefresh: function () {
    app.queryOrders().then(() => {
      this.refresh()
      wx.stopPullDownRefresh()
    })
  },
  refresh: function () {
    this.setData({
      orders: app.globalData.orders
    })
  },

  goOrderDetail: function (e) {
    // currentTarget 是指与当前事件绑定的组件的一些属性值集合
    // but target 这是指触发这个事件的组件的一些属性值集合
    // 由于事件的冒泡机制，很容易理解触发事件的组件和绑定事件处理函数的组件不是同一个组件
    let i = parseInt(e.currentTarget.id);
    app.globalData.currentOrder = this.data.orders[i];

    wx: wx.navigateTo({
      url: 'pages/order-detail/order-detail',
    })
  }
})
