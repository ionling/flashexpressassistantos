const regeneratorRuntime = require('../../../../utils/runtime')
const log = require('../../../../utils/log')
const app = getApp()


Page({
  /**
   * 页面的初始数据
   */
  data: {
    // UI design temp data
    // 是不是代取快递
    // isFetching: true,
    // order: {
    //   status: '已下单',
    //   price: 1.9,
    //   orderType: '代取快递'
    // },
    // address: {
    //   addressee: "张三",
    //   phone: "15328403080",
    //   apartment: 11,
    //   unit: 1,
    //   dormitory: 423,
    //   isDefault: true,
    //   other: ""
    // },
    // form: {

    // }

    honeyMap: [
      ['type', '类型'],
      ['amount', '数量'],
      ['comment', '备注']
    ],
    addressMap: [
      ['addressee', '收件人'],
      ['phone', '手机'],
      ['apartment', '寝室'],
      ['unit', '单元'],
      ['dormitory', '寝室'],
      ['other', '其他']
    ],
    fetchingMap: [
      ['company', '快递公司'],
      ['expressType', '快递类型'],
      ['pickupCode', '取货码'],
      ['informData', '通知时间'],
      ['remark', '备注']
    ],
    sendingMap: [
      ['company', '快递公司'],
      ['fetchTime', '取件时间'],
      ['expressNumber', '快递单号'],
      ['addresseePhone', '收件人手机'],
      ['addresseeName', '收件人'],
      ['addresseeAddress', '收件地址'],
      ['remark', '备注']
    ]
  },

  onLoad: async function () {
    // 1. 显示动画
    // 2. 拉取数据
    // 3. 数据渲染
    app.showLoading("加载中")
    const order = app.globalData.currentOrder
    const deletable = order.get('status') == '已下单'
    const orderType = order.get('orderType')
    let query
    let formMap
    if (orderType == '代取快递') {
      query = new app.bmob.Query(app.bmob.Fetching)
      formMap = this.data.fetchingMap
    } else if (orderType == '代寄快递') {
      query = new app.bmob.Query(app.bmob.Sending)
      formMap = this.data.sendingMap
    } else if (orderType == '蜂零零') {
      query = new app.bmob.Query(app.bmob.Honey)
      formMap = this.data.honeyMap
    }
    this.setData({
      deletable: deletable,
      order: order,
      formMap: formMap
    })

    query.equalTo('order', order)
    query.include('address')
    let step
    try {
      step = 'before query form'
      const form = await query.first()
      step = 'after query form'
      this.setData({
        form: form,
        address: form.get('address')
      })
      step = 'after set data'
      app.hideLoading("加载成功", 600)
      log.info("query order detail data successfully")
    } catch (error) {
      const info = '加载失败'
      log.error(info, 'in order-detail onLoad():', error, 'step:', step)
      app.hideLoading(info, 600)
    }
  },

  delete: function () {
    // 1. 检查订单状态：只有已下单的才能删除
    if (!this.data.deletable) {
      wx.showModal({
        title: '提示',
        content: '已取件的订单无法删除',
        showCancel: false
      })
      return
    }

    // 2. 发送云端请求
    wx.showModal({
      title: '提示',
      content: '小闪将不会进行配送了哦',
      success: async (res) => {
        if (res.confirm) {
          console.log("用户点击确定")
          app.showLoading("删除中")
          try {
            const form = this.data.form
            const orderQuery = new app.bmob.Query(app.bmob.Order)
            // orderQuery.get(...).destroy() is not a function;
            const order = await orderQuery.get(form.get('order').id)
            await form.destroy()
            await order.destroy()
            await app.queryOrders()
            app.hideLoading("删除成功", 200, wx.navigateBack)
            log.info("删除成功")
          } catch (error) {
            const info = '删除出错'
            app.hideLoading(info, 200, null, false)
            log.error(info, 'in order-detail delete()', error)
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  pay: async function () {
    app.showLoading('提交中')
    try {
      // 1. get response from Bmob
      const openId = app.globalData.user.get('authData').weapp.openid
      const order = this.data.order
      const response = await app.bmob.Pay.wechatPay(order.get('price'), order.get('orderType'), 'description', openId)
      // 订单号
      const orderNumber = response.out_trade_no
      app.hideLoading(null, 10, null, false)

      // 2. requestPayment
      await new Promise((resolve, reject) => {
        wx.requestPayment({
          'timeStamp': response.timestamp,
          'nonceStr': response.noncestr,
          'package': response.package,
          'signType': 'MD5',
          'paySign': response.sign,
          'success': async (res) => {
            try {
              await order.save({
                orderNumber: orderNumber,
                status: '已支付'
              })
              this.setData({
                order: order
              })
              console.log(res, '支付成功 in requestPayment')
              app.log('info', '付款成功', res)
              resolve(res)
            } catch (error) {
              log.error('exception in requestPayment success async func', error)
              reject(error)
            }
          },
          'fail': function (res) {
            const info = '付款失败'
            log.info(info, res)
            wx.showModal({
              title: '提示',
              content: info,
              showCancel: false
            })
            reject(res)
          }
        })
      })
      await app.queryOrders()
    } catch (error) {
      app.hideLoading(null, 200, null, false)
      if (error.errMsg != 'requestPayment:fail cancel')
        log.error('服务端返回失败 in order-detail pay()', error)
    }
  }
})
