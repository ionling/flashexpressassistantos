const regeneratorRuntime = require('../../../../utils/runtime')
const app = getApp()

Page({
    data: {
        version: app.globalData.version
    },

    clearStorage: function () {
        wx.showModal({
            title: '提示',
            content: '将清理缓存',
            success: async function (res) {
                if (res.confirm) {
                    app.showLoading('清理中')
                    try {
                        wx.clearStorageSync()
                        await app.onLaunch()
                        app.hideLoading('清理完成', 200, null, true)
                        console.log('清理完成')
                    } catch (e) {
                        app.hideLoading(null, 200)
                        app.showHint('清理完成')
                        app.log('warning', 'error with clearStorage() in about.js', e)
                    }
                }
            }
        })
    }
})
