let app = getApp()
let checker = require('../../../../utils/checker')

Page({
  /**
   * 页面的初始数据
   */
  data: {
    tcvalue: "在这里填写您对这个小程序的吐槽或建议，我们都愿闻其详,您的提议或许是我们进步的一块路标"
  },

  formSubmit: function (e) {
    let value = e.detail.value
    checker.checkFeedback(value).then(() => {
      let feedback = new app.bmob.Feedback()
      return feedback.save({
        user: app.globalData.user,
        content: value.content,
        qq: +value.qq,
        status: 2
      })
    }).then(() => {
      console.log("feedback.save() succeed!")
      wx.redirectTo({
        url: '../feedback-success/feedback-success'
      })
    }, (error) => {
      wx.showModal({
        title: '提示',
        // checkFeedback 的 Promise 返回的是字符串，就不用toString了
        content: error,
        showCancel: false,
      })
      // console.error()不会终止程序的运行，所以那就用吧
      console.error(error)
    })
  }
})
