const app = getApp()
const regeneratorRuntime = require('../../utils/runtime')

Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    list: [{
        id: 'order',
        name: '我的订单',
        page: '/pages/order/index'
      },
      {
        id: 'address',
        name: '地址管理',
        page: '/pages/address/index'
      },
      {
        id: 'feedback',
        name: '吐槽这个小程序',
        page: 'pages/feedback/feedback'
      },
      {
        id: 'about',
        name: '关于',
        page: 'pages/about/about'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    this.refresh()
  },

  // 下拉刷新
  onPullDownRefresh: async function () {
    // 刷新缓存(Bmob.User.current())，不会影响界面
    await app.logIn()
    wx.stopPullDownRefresh()
  },

  refresh: function () {
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },
})
