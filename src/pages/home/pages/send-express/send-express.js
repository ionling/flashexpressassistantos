const regeneratorRuntime = require('../../../../utils/runtime')
const log = require('../../../../utils/log')
let app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 快递公司
    companies: ['默认合作快递', '中通', '申通', '韵达', '圆通', '顺丰', '百世汇通', '天天', '东骏', '京东', '邮政EMS', '宅急送', '优速', '安能', '唯品会', '丰程', '万象', '品骏'],
    companyIndex: 0,
    //取件时间
    intervals: ['12:30-13:30', '18:30-19:30'],
    timeIndex: 0,
  },

  onLoad: async function () {
    const query = new app.bmob.Query(app.bmob.Data)
    try {
      const time = await query.get('BQdtKKKN')
      const intervals = await query.get('4ZYN000P')
      this.setData({
        timeReminder: time.get('value'),
        intervals: JSON.parse(intervals.get('value'))
      })
    } catch (error) {
      log.error(`Error in send-express onLoad(): ${error}`)
    }
  },
  onShow: function () {
    if (!app.globalData.defaultAddress) {
      wx.showModal({
        title: '提示',
        content: '您还没有默认地址，无法为你代寄快递',
        showCancel: false,
        success: () => {
          wx.navigateBack()
        }
      })
    }
  },
  companyChange: function (e) {
    this.setData({
      companyIndex: e.detail.value
    })
  },
  timeChange: function (e) {
    this.setData({
      timeIndex: e.detail.value
    })
  },
  formSubmit: async function (e) {
    let order = new app.bmob.Order();
    let sending = new app.bmob.Sending();
    const value = e.detail.value
    app.showLoading('添加中')
    try {
      order = await order.save({
        user: app.globalData.user,
        orderType: "代寄快递",
        status: "已下单"
      })
      sending = await sending.save({
        order: order,
        company: this.data.companies[value.company],
        fetchTime: this.data.intervals[value["time"]],
        addresseeName: value.receiveName,
        addresseeAddress: value["receiveAddress"],
        addresseePhone: parseInt(value["receiveTel"]),
        remark: value["remarks"],
        address: app.globalData.defaultAddress
      })
      console.log("sending.save() succeed!")
      app.hideLoading("添加成功", 200, wx.redirectTo({
        url: '../submit-success/submit-success'
      }))
    } catch (error) {
      await order.destroy()
      await sending.destroy()
      const info = '提交失败'
      app.hideLoading(info, 200, null, false)
      log.error("wrong with send-express", error)
      app.showHint(info)
    }
  }
})
