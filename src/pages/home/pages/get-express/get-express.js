const app = getApp()
const util = require('../../../../utils/util')
const regeneratorRuntime = require('../../../../utils/runtime')
const log = require('../../../../utils/log')


Page({
  /**
   * 页面的初始数据
   */
  data: {
    timeReminder: 'This is reminder',
    pickupCodeReminder: '1. 如果是申通、顺风、京东，请填写电话后四位\n2. “4-5-1025”，请填写为 451025\n3. 请大家核对后填写！！',
    companyReminder: '1. 惠民路 328 号菜鸟驿站为百世快递\n2. 五福西巷 76 号为申通快递',
    typeIndex: 0,
    expressTypes: ['大件物品', '小件物品', '易碎品', '文件', '服饰', '书籍', '其他', '涉及隐私不便填写'],
    companyIndex: 0,
    companies: ['申通', '中通', '韵达', '圆通', '顺丰', '百世汇通', '天天', '东骏', '京东', '邮政EMS', '宅急送', '优速', '安能', '唯品会', '丰程', '万象', '品骏'],
    informDate: util.formatTime(new Date())
  },

  onLoad: async function () {
    const query = new app.bmob.Query(app.bmob.Data)
    try {
      const result = await Promise.all([query.get('2UoWiiin'), query.get('7jG1RRRN')])
      this.setData({
        timeReminder: result[0].get('value'),
        pickupCodeReminder: result[1].get('value')
      })
    } catch (error) {
      log.error(`Error in get-express onLoad(): ${error}`)
    }
  },

  onShow: function () {
    if (!app.globalData.defaultAddress) {
      wx.showModal({
        title: '提示',
        content: '您还没有默认地址，无法将快递送到你手上',
        showCancel: false,
        success: () => {
          wx.navigateBack()
        }
      })
    }
  },

  companyChange: function (e) {
    this.setData({
      companyIndex: e.detail.value
    })
  },
  typeChange: function (e) {
    this.setData({
      typeIndex: e.detail.value
    })
  },
  dateChange: function (e) {
    this.setData({
      informDate: e.detail.value
    })
  },

  formSubmit: async function (e) {
    // 不能使用箭头函数，否则 this === undefined
    const order = new app.bmob.Order()
    const fetching = new app.bmob.Fetching()
    const value = e.detail.value
    let step
    app.showLoading("提交中")
    const date = new Date(util.parseTime(value.informDate))

    try {
      step = 'before order save'
      await order.save({
        user: app.globalData.user,
        orderType: "代取快递",
        status: "已下单"
      })
      step = 'after order save'
      await fetching.save({
        order: order,
        company: this.data.companies[value.companyIndex],
        pickupCode: value.pickupCode,
        informDate: date,
        expressType: this.data.expressTypes[value.typeIndex],
        remark: value.remark,
        address: app.globalData.defaultAddress
      })
      step = 'after fetching save'
      log.info("fetching.save() succeed!")
      app.hideLoading("提交成功", 200, wx.redirectTo({
        url: '../submit-success/submit-success'
      }))
    } catch (error) {
      await order.destroy()
      await fetching.destroy()
      const info = '提交失败'
      log.error('error with get-express:', error, 'step:', step, 'date:', date)
      app.showHint(info)
    }
    app.hideLoading(null, 200, null, false)
  }
})
