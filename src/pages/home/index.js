const log = require('../../utils/log')
const regeneratorRuntime = require('../../utils/runtime')
const app = getApp()

Page({
  data: {
    logoSrc: "../../image/logo.png",
    list: [{
        id: 'get',
        name: '代取快递',
        page: 'get-express',
        available: false,
      },
      {
        id: 'send',
        name: '代寄快递',
        page: 'send-express',
        available: true,
      },
    ],
  },
  onLoad: async function () {
    const query = new app.bmob.Query(app.bmob.Data)
    try {
      const list = await query.get('OkoC666D')
      this.setData({
        list: JSON.parse(list.get('value')),
      })
    } catch (error) {
      log.error(`Error in home/index onLoad(): ${error}`)
    }
  },
  ban: function (event) {
    console.log(event)
    wx.showModal({
      title: '提示',
      content: '该功能暂时不可用',
      showCancel: false,
      success: function (res) {
        if (res.confirm) {
          wx.navigateBack()
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }
})
